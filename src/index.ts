import { setupServer } from "msw/node";
import { rest } from "msw";

const notFoundByDefault = rest.get(/.*/, (req, res, ctx) =>
  res(ctx.status(404))
);

export const server = () => setupServer(notFoundByDefault);
