Example project for issue: https://github.com/mswjs/msw/issues/408

1. clone
1. `npm i`
1. `npm run compile`

# TypeScript requires `dom` library even for `node` only projects.

## Environment

| Name | Version |
| ---- | ------- |
| msw  | 0.21.2   |
| node | 12.16.3     |
| OS   | macOS catalina 10.15.7 (19H2)     |

## Problem

The `msw` TypeScript compilation requires `dom` library even for `node` only projects.

`tsconfig.json`
```diff
  {
    "compilerOptions": {
      "module": "commonjs",
      "target": "es6",
      "outDir": "out",
-     "lib": ["es6", "dom"],
+     "lib": ["es6"],
      "sourceMap": true,
      "strict": true,
    },
    "include": ["src/**/*"]
  }
```

## Current behavior

When the `dom` library is missing, TS can't resolve types like `Request`, `URL`, and `ServiceWorker`.


```
node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts:9:82 - error TS2304: Cannot find name 'RequestInit'.

9     fetch: (input: string | MockedRequest<DefaultRequestBodyType>, requestInit?: RequestInit) => any;
                                                                                   ~~~~~~~~~~~

node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts:13:10 - error TS2304: Cannot find name 'URL'.

13     url: URL;
            ~~~

node_modules/msw/lib/types/utils/handlers/requestHandler.d.ts:14:13 - error TS2304: Cannot find name 'Request'.

14     method: Request['method'];
               ~~~~~~~
```

## Expected behavior

TypeScript should compile `msw` without `dom` library when used only for a node project.

## Screenshots

Example project: https://gitlab.com/viktomas/showcase-msw-bug